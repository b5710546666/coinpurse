package coinpurse;

/**
 * A banknote with a monetary value.
 * You can't change the value of a banknote.
 * @author Apichaya Bunchongchit
 */

public class BankNote extends AbstractValuable {
	/**serial number of each banknote*/
	private static int nextSerialNumber = 1000000;
	
	/** 
	 * Constructor for a new banknote. 
	 * @param value is the value for the banknote.
	 */
	public BankNote ( double value , String currency ){
		super( value , currency); // this is the automatic behavior in Java
		this.nextSerialNumber = getNextSerialNumber();
	}
	
	/**
	 * getting next serial number ,by adding last serial number by one
	 * @return next serial number
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber++;
	}

	public String toString(){
		String toReturn = String.format("%.0f-%s Banknote [%d]",value,currency,this.nextSerialNumber);

		return toReturn;
	}
}
