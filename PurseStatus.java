package coinpurse;

import java.awt.Container;
import java.awt.Font;
import java.awt.Label;
import java.util.Observable; 
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class PurseStatus extends JFrame implements Observer{

	private JPanel panel ;
	private Label label ;
	private JProgressBar progressBar ;

	public PurseStatus (){
		super("PurseStatus"); // super = JFrame
		initComponents();
	}

	public void update(Observable subject, Object info) {
		Purse purse = (Purse) subject;
		if (subject instanceof Purse){
			progressBar.setMinimum(0);
			progressBar.setMaximum(purse.getCapacity());
			progressBar.setValue(purse.count());
			
			if (purse.isFull())	label.setText("Full");
			else if (purse.count()==0)	label.setText("Empty");
			else label.setText( " " + purse.count() );
		}
		progressBar.setValue(purse.count());
	}

	public void initComponents(){
		Font font = new Font("Arial",Font.ITALIC,30);
		
		panel = new JPanel();
		
		label = new Label();
		label.setFont(font);
		panel.add(label);
		
		progressBar = new JProgressBar();
		panel.add(progressBar);
				
		this.add(panel);
	}

	public void run(){
		this.setVisible(true);
		this.pack();
	}

}