package coinpurse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A coin purse contains money. You can insert money, withdraw money,
 * check the balance, and check if the purse is full. When you withdraw money
 * purse decides which coins to remove.
 * 
 * @author Apichaya Bunchongchit 
 */
public class Purse extends Observable{
	/** Collection of Valuable in the purse. */
	private List<Valuable> list;

	/**
	 * Capacity is maximum NUMBER of items the purse can hold. Capacity is set
	 * when the purse is created.
	 */
	private int capacity;
	/**
	 * Strategy withdraw.
	 */
	private WithdrawStrategy toWithdraw;
	/**
	 * Create a purse with a specified capacity.
	 * 
	 * @param capacity
	 *            is maximum number of items you can put in purse.
	 */
	public Purse(int capacity) {
		this.capacity = capacity;
		list = new ArrayList<Valuable>();
	}
	/**
	 * Count and return the number of money in the purse. This is the number of
	 * money, not their value.
	 * 
	 * @return the number of money in the purse
	 */
	public int count() {
		return list.size();
	}
	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for (int i = 0; i < list.size(); i++) {
			balance += list.get(i).getValue();
		}
		return balance;
	}
	/**
	 * Return the capacity of the items purse.
	 * 
	 * @return the capacity
	 */
	public int getCapacity() {
		return this.capacity;
	}
	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * 
	 * @return true if purse is full.
	 */
	public boolean isFull() {
		if (this.count() >= this.capacity) {
			return true;
		}
		return false;
	}
	/**
	 * Insert a Valuable into the purse. The Valuable is only inserted if the purse has
	 * space for it and the money has positive value. No worthless money!
	 * 
	 * @param valuable
	 *            is a Valuable object to insert into purse
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert(Valuable valuable) {
		/** if the purse is already full then can't insert anything.*/
		if (this.isFull()) {
			return false;
		} else {
			list.add(valuable);
			/**	notify the observers*/
			super.setChanged();
			super.notifyObservers(this);
		}
		return true;
	}
	/**
	 * Withdraw the requested amount of money. Return an array of Valuable
	 * withdrawn from purse, or return null if cannot withdraw the amount
	 * requested.
	 * @param amount is the amount to withdraw
	 * @return array of Valuable objects for money withdrawn, or null if cannot 
	 * withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount) {	
		toWithdraw = new GreedyWithdraw();
		if (amount < 0)		return null;
		if (this.getBalance()<amount) return null;
		Valuable[] toReturn = toWithdraw.withdraw(amount, this.list);
		
		if (toReturn==null){
			this.setWithdrawStrategy(new RecursiveWithdraw());
			toReturn = toWithdraw.withdraw(amount, this.list);
		}
		
		if (toReturn!=null){
			/**	notify the observers*/
			super.setChanged();
			super.notifyObservers(this);				
		}
		
		return toReturn;
	}
	/**
	 * to set type of WithdrawStrategy.
	 * @param type of WithdrawStrategy
	 * can be GreedyWithdraw or RecursiveWithdraw.
	 */
	public void setWithdrawStrategy(WithdrawStrategy toWithdraw){
		this.toWithdraw = toWithdraw;
	}
	
	/**
	 * toString returns a string description of the purse contents. It can
	 * return whatever is a useful description.
	 */
	public String toString() {
		int amount = this.count();
		double balance = this.getBalance();
		return String.format("%d items with value %.1f", amount, balance);
	}
}