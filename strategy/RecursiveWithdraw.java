package coinpurse.strategy;
import java.util.ArrayList; 
import java.util.Collections;
import java.util.Comparator;
import java.util.List; 
import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * Some cases of withdrawal by Greedy strategy have mistakes.
 * They don't withdraw even actually they can.
 * To recursive make sure that they have take all chance to withdraw. 
 * @author Apichaya Bunchongchit
 */

public class RecursiveWithdraw implements WithdrawStrategy{

	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> pocket = withdrawHelp(amount,valuables,new ArrayList<Valuable>());
		Valuable[] toReturn = new Valuable[pocket.size()];

		/**
		 * new List created to work as artificial valuables.
		 */
		List<Valuable> temp = new ArrayList<Valuable>(valuables);
		
		/**
		 * to remove coin from purse
		 */
		for (int i = 0 ; i<pocket.size() ; i++){
			for (int j = 0 ; j<temp.size() ; j++){
				if (pocket.get(i).getValue()==temp.get(j).getValue()){
					/**remove obj , by checking obj that equals obj in param instead position
					 * in case of using position , when obj has been remove , size will smaller.
					 * so there occurs some mistakes caused by position.
					 */
					valuables.remove(temp.get(j));
				}
			}
		}

		pocket.toArray(toReturn);
		return toReturn;
	}

	/**
	 * 
	 * @param amount : double - money that ask to withdraw.
	 * @param valuables : List<Valuable> - a purse that keep all money.
	 * @param pocket : List<Valuable> - a pocket that collect money to return.
	 * @return List<Valuable>
	 */
	public List<Valuable> withdrawHelp(double amount,List<Valuable> valuables,List<Valuable> pocket) {

		if (amount==0) return pocket;
		if (amount>0 && valuables.size()==0) return null;
		if (amount<0) return null;

		pocket.add(valuables.get(0));
		List<Valuable> temp = withdrawHelp(amount-valuables.get(0).getValue(),valuables.subList(1,valuables.size()),pocket);

		if (temp==null){
			pocket.remove(pocket.size()-1);
			pocket = withdrawHelp(amount,valuables.subList(1, valuables.size()),pocket);			
		}		
		return pocket;
	}

}
