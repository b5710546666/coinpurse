package coinpurse;

import java.util.HashMap;

import com.sun.javafx.collections.MappingChange.Map;

/*
 * @author Apichaya Bunchongchit
 */
public class Coupon extends AbstractValuable {

	/** coupon's color. */
	private String color;
	/** giving value of each coupon by color. */
	private static HashMap<String, Double> map;
	static{	
		map = new HashMap<String,Double>();
		map.put("red",100.0);
		map.put("blue",50.0);
		map.put("green",20.0);
	}
	
	/** 
	 * Constructor for a new coupon. 
	 * @param color for the coupon
	 */
	public Coupon(String color){
		this.color = color.toLowerCase();
		super.setValue(map.get(this.color));;
		
	}
	
	/** 
	 * get value of coupon.
	 * @return value of coupon.
	 */
	public double getValue() {
		return this.value;
	}
	
	/**compare two coupons.
	 * @param Obj
	 * @return true when the coupons have same color
	 */
	public boolean equals(Object obj) {
		if (obj == null) return false;

		if ( obj.getClass() != this.getClass() )	return false;

		Coupon other = (Coupon) obj;
		if (this.color.equals(other.color))	return true;
		
		return false;
	}
	
	public String toString (){
		return this.color + " coupon";
	}
	
}
