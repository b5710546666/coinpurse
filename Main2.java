package coinpurse;

public class Main2 {

	public static void main ( String[] args ){
		MoneyFactory factory = MoneyFactory.getInstance();
	
		System.out.println( factory.createMoney("10"));
		System.out.println( factory.createMoney(10.0));
		
	}
	
}
