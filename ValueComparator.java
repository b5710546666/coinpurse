package coinpurse;

import java.util.Comparator;
/**
 * to comepare two valuables
 * @author Apichaya Bunchongchit
 */
public class ValueComparator implements Comparator<Valuable> {
	public int compare(Valuable a, Valuable b) {
		
		if (a.getValue()>b.getValue()){
			return 1;
		}
		else if (a.getValue()==b.getValue()){
			return 0;			
		}
		else {
			return -1;
		}
		// compare them by value.  This is easy.
	}
}

