package coinpurse;

/**
 * Super class of Coin , Banknote and Coupon.
 * @author Apichaya Bunchongchit
 */

public abstract class AbstractValuable implements Valuable {
	
	String currency;
	/** Value of the object. */
	protected double value;	
	
	/**
	 * Initialize the value of a new instance of some Valuable.
	 * @param value the value of this Valuable
	 */
	public AbstractValuable(double value , String currency) {
		this.value = value;
		this.currency = currency;
	}

	public AbstractValuable() {
		super();
	}
	
	/**
	 * to check these object equals or not.
	 * @param Object , another object we want to check.
	 * @return true if they equals.
	 */
	public boolean equals(Object obj){

		if (obj == null) return false;
		else if (this.getClass() != obj.getClass())	return false;

		Valuable test1 = (Valuable) this;
		Valuable test2 = (Valuable) obj;
	
		if (test1.getValue() != test2.getValue()) return false;
		return true;
	}

	/**
	 * to check these object have the same value or not.
	 * @param Valuable , another Valueable we want to check.
	 * @return true if they have the same value.
	 */
	public int compareTo(Valuable value){

		if (this.equals(value)) {
		Valuable test = (Valuable) this;
		if (test.getValue() > value.getValue()) return 1;
		else if (test.getValue() == value.getValue()) return 0;
		}
		
		return -1;
	}

	/**
	 * getting value of coin
	 * @return value of the coin
	 */
	public double getValue() {
		return this.value; 
	}
	
	protected void setValue(double value) {
		this.value = value;
	}

}