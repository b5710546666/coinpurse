package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Apichaya Bunchongchit 5710546666
 */
public class Coin extends AbstractValuable {

	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value , String currency) {
		super(value , currency);
	}


	/**
	 * compare value of two coins.
	 * @param coin
	 * @return +1 if this coin is greater , -1 if other coin is greater or be null and 0 for other cases;
	 */

	
	 public String toString() {
	
	    return this.value + "-" + currency + " coin";
	    	
	 }
}