package coinpurse;

import java.util.ResourceBundle;

public  abstract class MoneyFactory {
	
	private static MoneyFactory instance;
	
	public static MoneyFactory getInstance(){
		ResourceBundle bundle = ResourceBundle.getBundle("currency");
		String value = bundle.getString("default");
		if (instance == null){
			try {
				instance = (MoneyFactory) Class.forName(value).newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	abstract Valuable createMoney(double value);

	Valuable createMoney(String value) {
		return createMoney(Double.parseDouble(value));
	}
	
	
}
