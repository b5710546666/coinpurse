//package coinpurse;
//
//
///**
// * A main class to create objects and connect objects together.
// * The user interface needs a reference to coin purse.
// */
//public class Main {
//
//    /**
//     * @param args not used
//     */
//    public static void main( String[] args ) {
//    	Purse purse = new Purse(10);
//    	PurseBalance purseBalance = new PurseBalance();
//    	PurseStatus purseStatus = new PurseStatus();
//    	purse.addObserver(purseBalance);
//    	purse.addObserver(purseStatus);
//    	
//    	ConsoleDialog ui = new ConsoleDialog( purse );
//    	purseStatus.run();
//    	purseBalance.run();
//    	
//    	ui.run();
//    }
//}
