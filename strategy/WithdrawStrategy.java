package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

public interface WithdrawStrategy {

	public Valuable[] withdraw(double amount , List<Valuable> valuables);
	
}
