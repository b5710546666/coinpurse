package coinpurse;

import java.awt.Container;
import java.awt.Font;
import java.awt.Label;
import java.util.Observable; 
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PurseBalance extends JFrame implements Observer{

	private JPanel panel ;
	private Label label ;

	public PurseBalance (){
		super("PurseBalance"); // super = JFrame
		initComponents();
	}

	public void update(Observable subject, Object info) {
		if (subject instanceof Purse){
			Purse purse = (Purse) subject;
			int balance = (int) purse.getBalance();
			label.setText(balance + " baht");
		}

		if (info != null) System.out.println(info);
	}

	public void initComponents(){

		super.setLocation(200, 200);
		Font font = new Font("Arial",Font.ITALIC,30);
		panel = new JPanel();
		
		label = new Label();
		label.setFont(font);
		panel.add(label);
		
		this.add(panel);
	}

	public void run(){
		this.setVisible(true);
		this.pack();
	}

}