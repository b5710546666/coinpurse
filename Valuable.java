package coinpurse;

/**
 *Interface that has a method to get value
 */

public interface Valuable extends Comparable<Valuable> {
	/**
	 * to return value of object
	 */
	public double getValue();
	
}
