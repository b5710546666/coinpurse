package coinpurse;

public class MalaiMoneyFactory extends MoneyFactory{

	Valuable createMoney(double value) {

		if (value == 0.05) return new Coin(0.05,"Sen");
		if (value == 0.10)	return new Coin(0.10,"Sen");
		if (value == 0.20) return new Coin(0.20,"Sen");
		if (value == 1) return new BankNote(1,"Ringgit");
		if (value == 2) return new BankNote(2,"Ringgit");
		if (value == 5)	return new BankNote(5,"Ringgit");
		if (value == 10) return new BankNote(10,"Ringgit");
		if (value == 20) return new BankNote(20,"Ringgit");
		if (value == 100) return new BankNote(100,"Ringgit");
		else return null;	
	}
}
