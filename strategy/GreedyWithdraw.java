package coinpurse.strategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

public class GreedyWithdraw implements WithdrawStrategy {

	private final ValueComparator comparator = new ValueComparator();
	
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
	
		Collections.sort( valuables , comparator);
		List<Valuable> test = new ArrayList<Valuable>();
		test.addAll(valuables);
		List<Valuable> temp = new ArrayList<Valuable>();
		for (int i = valuables.size()-1; i >= 0 && amount >= 0; i--) {
			if (amount >= valuables.get(i).getValue()) {
				amount -= valuables.get(i).getValue();
				temp.add(valuables.get(i));
				test.remove(i);
			}
		}
		if (amount > 0)	return null;
		Valuable[] toWithdraw = new Valuable[temp.size()];
		for (int i = 0; i < temp.size(); i++) {
			toWithdraw[i] = temp.get(i);
		}
		for (int i = 0 ; i < temp.size() ; i++ ){
			valuables.remove(toWithdraw[i]);
		}
		return toWithdraw;
	}
}
