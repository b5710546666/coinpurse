package coinpurse;

public class ThaiMoneyFactory extends MoneyFactory{

	@Override
	Valuable createMoney(double value) {

		switch ( (int)value ) {
		case 1: case 2: case 5: case 10:
			return new Coin(value,"Baht");
		case 20: case 50: case 100: case 500: case 1000:
			return new BankNote(value,"Baht");
		default: return null;
		}
	}

}
